import abilityPick from './images/abilityPick.jpg';
import slide1 from './images/slide_1.jpg';
import slide2 from './images/slide_2.jpg';
import slide3 from './images/slide_3.jpg';
import slide4 from './images/slide_4.jpg';
import poster from './images/videoPick.jpg';

import adv1 from './icons/adv1.svg';
import adv2 from './icons/adv2.svg';
import adv3 from './icons/adv3.svg';
import adv4 from './icons/adv4.svg';
import lift from './icons/lift.svg';
import radio from './icons/radio.svg';
import transport from './icons/transport.svg';

import video from './videos/long.mp4';

export { abilityPick, adv1, adv2, adv3, adv4, lift, poster, radio, slide1, slide2, slide3, slide4, transport, video};