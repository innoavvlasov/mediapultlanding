import styled from 'styled-components';

const calcResponsive = (min, max) =>
  `calc(${min}px + (${max} - ${min}) * ((100vw - 360px) / (1050 - 360)))`;

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: auto;
  box-sizing: border-box;
  margin-bottom: 30px;
`;

export const ContentBlock = styled.div`
  display: grid;
  grid-template-columns: minmax(330px, 1050px);
  grid-template-rows: 50px auto;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin-top: 10px;
  margin-left: 15px;
  margin-right: 15px;
  @media (min-width: 768px) {
    margin-top: 300px;
    grid-template-rows: 100px auto;
  }
`;

export const HeaderLine = styled.header`
  color: #354052;
  > h1 {
    font-size: ${calcResponsive(12, 22)};
    line-height: 1.2;
    font-weight: 400;
  }
  @media (min-width: 768px) {
    h1 {
      font-size: ${calcResponsive(26, 30)};
    }
  }
`;

export const AbilityBlock = styled.div`
  display: grid;
  grid-template-rows: auto auto;
  grid-template-columns: auto;
  @media (min-width: 768px) {
    grid-template-columns: auto auto;
    margin-top: 20px;
  }
`;

export const StyledImg = styled.div`
  display: grid;
  grid-template-columns: auto;
  img {
    text-align: center;
    align-items: center;
    margin: 0 auto;
    width: 50vw;
    @media (min-width: 768px) {
      width: 100%;
    }
  }
`;

export const DescriptionAbility = styled.div`
  display: grid;
  grid-template-rows: auto auto auto;
  grid-row-gap: 3vw;
  position: relative;
  align-self: stretch;
  padding: 3vw 4vw;
  @media (min-width: 768px) {
    padding: 0 15px;
  }
`;
export const Row = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-column-gap: 15px;
`;
export const Circle = styled.div`
  border: 1px #01b7f4 solid;
  border-radius: ${calcResponsive(30, 30)};
  font-size:  ${calcResponsive(15, 17)};
  padding: ${calcResponsive(6, 8) + ' ' + calcResponsive(12, 14)};
  color: #01b7f4;
  text-align: center;
  @media screen and (min-width: 2048px) {
    border-radius:30px;
    font-size:  22px;
    padding: 10px 18px;
  }
`;

export const Desc = styled.div`
  color: black;
  font-size: ${calcResponsive(15, 17)};
  line-height: 1.2;
  font-weight: 400;
  text-align: left;
`;
