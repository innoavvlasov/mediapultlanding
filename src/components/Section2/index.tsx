import * as React from 'react';

import {
  Wrapper,
  ContentBlock,
  HeaderLine,
  AbilityBlock,
  DescriptionAbility,
  Row,
  Circle,
  Desc,
  StyledImg
} from './styled';

import { abilityPick } from 'src/assets';

export class SecondSection extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        <ContentBlock>
          <HeaderLine>
            <h1>
              Media Pult – это сервис агрегатор для размещение <br />
              традиционной рекламы через окно вашего браузера.
            </h1>
          </HeaderLine>
          <AbilityBlock>
            <StyledImg>
              <img src={abilityPick} />
            </StyledImg>
            <DescriptionAbility>
              <Row>
                <div>
                  <Circle>1</Circle>
                </div>
                <Desc>
                  Не покидая своего рабочего места, вы сможете разместить
                  рекламный ролик на радио, запустить рекламу на транспорте, в
                  лифтах жилых домов и бизнес центров.
                </Desc>
              </Row>
              <Row>
              <div><Circle>2</Circle></div>
                <Desc>
                  Раньше для того, чтобы запустить рекламу вам необходимо было
                  связываться с десятками рекламных подрядчиков, направлять
                  запросы по электронной почте, чтобы получить актуальные
                  предложения и цены. Теперь всё это доступно в рамках нашего
                  онлайн сервиса.{' '}
                </Desc>
              </Row>
              <Row>
              <div><Circle>3</Circle></div>
                <Desc>
                  Media Pult – это только актуальные цены от непосредственных
                  владельцев медиа ресурсов. Наш сервис исключает посредников в
                  цепочке, экономит время и деньги рекламодателей.
                </Desc>
              </Row>
            </DescriptionAbility>
          </AbilityBlock>
        </ContentBlock>
      </Wrapper>
    );
  }
}
