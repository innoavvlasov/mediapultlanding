import { Logo } from './logo';
import { FirstSection } from './Section1';
import { SecondSection } from './Section2';
import { ThirdSection } from './Section3';
import { FourthSection } from './Section4';
import { FiveSection } from './Section5';

export {Logo, FirstSection, SecondSection, ThirdSection, FourthSection, FiveSection}