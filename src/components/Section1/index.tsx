import * as React from 'react';
import { Logo } from '../logo';
import Identification from '../identification';

import {
  BackGround,
  StyledNav,
  HeaderLine,
  SubMenu,
  SubButtonLeft,
  SubButtonRight,
  SubButtonCenter,
  ContentBlock,
  VideoBlock,
  PlayVideo
} from './styled';

import { poster, video } from 'src/assets';

export class FirstSection extends React.PureComponent <any, any> {
  openAPP = (e, app) => {
    e.preventDefault();
    this.props.openApp(app);
  }

  render() {
    return (
      <BackGround>
        <StyledNav>
          <Logo />
          <Identification {...this.props}/>
        </StyledNav>
        <ContentBlock>
          <HeaderLine>
            <h1>MediaPult - сервис агрегатор оффлайн рекламы.</h1>
            <h2>
              Предложения и актуальные цены на традиционную рекламу здесь и
              сейчас.
            </h2>
          </HeaderLine>
          <SubMenu>
            <SubButtonLeft href="#" onClick={(e) => this.openAPP(e, 'radio/radioapp')}>Реклама на радио</SubButtonLeft>
            <SubButtonCenter href="#" onClick={(e) => this.openAPP(e, 'transport-landing/mdtlandingapp')}>Реклама на транспорте</SubButtonCenter>
            <SubButtonRight href="#" onClick={(e) => this.openAPP(e, 'elevators/elevators')}>Реклама в лифтах</SubButtonRight>
          </SubMenu>
        </ContentBlock>
          <VideoBlock controls={true} poster={poster}>
            <source src={video} type={'video/mp4'} />
            Your browser does not support the video tag.
          </VideoBlock>
          <PlayVideo>Посмотреть ролик</PlayVideo>
      </BackGround>
    );
  }
}
