import styled from 'styled-components';
// 1050 максимальный размер блока с контентом 
// 360 минимальный размер блока с контентом 
const calcResponsive = (min, max) =>
  `calc(${min}px + (${max} - ${min}) * ((100vw - 360px) / (1050 - 360)))`;

export const BackGround = styled.div`
  height: 300px;
  box-sizing: border-box;
  background: linear-gradient(
    270deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );

  @media (min-width: 768px) {
    height: 550px;
  }
`;
export const StyledNav = styled.nav`
  display: grid;
  grid-template-columns: 50% 50%;
  justify-content: space-between;
  margin-left: 15px;
  margin-right: 15px;
`;

export const ContentBlock = styled.div`
  display: grid;
  grid-template-columns: minmax(330px, 1050px);
  grid-template-rows: 80px 60px;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin-top: 25px;
  margin-left: 15px;
  margin-right: 15px;

  @media (min-width: 768px) {
    grid-template-rows: 100px 100px;
  }
`;

export const HeaderLine = styled.header`
  color: white;
  h1 {
    font-size: ${calcResponsive(14, 32)};
    font-weight: 500;
  }

  h2 {
    font-size: ${calcResponsive(10, 16)};
    font-weight: 300;
  }

  @media (min-width: 768px) {
    h1 {
      font-size: ${calcResponsive(26, 30)};
    }

    h2 {
      font-size: ${calcResponsive(15, 17)};
    }
  }

  @media screen and (min-width: 2048px) {
    h1 {
      font-size: 36px;
    }
    h2 {
      font-size: 22px;
    }
  }
`;

export const SubMenu = styled.nav`
  display: grid;
  align-items: center;
  justify-content: center;
  grid-template-columns: minmax(100px, 150px) minmax(120px, 180px) minmax(
      100px,
      150px
    );
  @media (min-width: 768px) {
    grid-template-columns: minmax(100px, 215px) minmax(120px, 270px) minmax(
        100px,
        215px
      );
  }
  @media screen and (min-width: 2048px) {
    grid-template-columns: 220px 280px 220px;
  }
`;

const SubButton = styled.a`
  background: none;
  color: white;
  border: none;
  font-size: ${calcResponsive(9, 14)};
  text-decoration: none;
  padding: ${calcResponsive(5, 8) + ' ' + calcResponsive(5, 5)};

  &:hover {
    background-color: rgba(255, 255, 255, 0.2);
  }

  &:active {
    background-color: rgba(255, 255, 255, 1);
    color: #1991eb;
  }

  @media screen and (min-width: 2048px) {
    font-size: 22px;
    padding: 10px 10px;
  }
`;
export const SubButtonCenter = styled(SubButton)`
  border-top: 1px white solid;
  border-bottom: 1px white solid;
  @media (min-width: 768px) {
    border-top: 2px white solid;
    border-bottom: 2px white solid;
  }
`;

export const SubButtonLeft = styled(SubButton)`
  border: 1px white solid;
  @media (min-width: 768px) {
    border: 2px white solid;
  }
  border-top-left-radius: 3vw;
  border-bottom-left-radius: 3vw;
`;
export const SubButtonRight = styled(SubButton)`
  border: 1px white solid;

  @media (min-width: 768px) {
    border: 2px white solid;
  }
  border-top-right-radius: 3vw;
  border-bottom-right-radius: 3vw;
`;

export const VideoBlock = styled.video`
  display: none;
  @media (min-width: 768px) {
    display: block;
    width: 100%;
    max-width: 800px;
    position: relative;
    margin: 0 auto;
    margin-top: 30px;
    box-shadow: 0px 20px 40px 0px rgba(0, 0, 0, 0.24);
  }
`;

export const PlayVideo = styled.div`
  display: block;
  color: white;
  font-size: ${calcResponsive(9, 14)};
  font-weight: 300;
  width: 100%;
  text-align: center;
  margin-top: 50px;
  @media (min-width: 768px) {
    display: none;
  }
`;
