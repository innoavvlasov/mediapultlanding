import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import * as Thunk from 'redux-thunk';
import { createGlobalStyle } from 'styled-components';
import { FirstSection, SecondSection, ThirdSection, FourthSection, FiveSection} from './components';
import { Wrapper } from './styled';

const GlobalStyle = createGlobalStyle`
* {
    margin: 0;
    padding: 0;
    font-family: 'Montserrat', sans-serif;
  }

  html, body {
    position: relative;
	  width: 100vw;
	  overflow-x: hidden !important;
  }
  
  @keyframes wave {
    50% {
      transform: scale(0.9);
    }
  }
`;

export class App extends React.PureComponent {
  render () {
    return (
      <Wrapper>
        <FirstSection {...this.props}/>
        <SecondSection />
        <ThirdSection {...this.props}/>
        <FourthSection />
        <FiveSection />
        <GlobalStyle />
      </Wrapper>
    );
  }
};

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch) => ({})

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App)


export default () => (
  <Provider store={createStore((state = { stateName: 'startManager' }, action) => state, composeWithDevTools(applyMiddleware(Thunk.default)))}>
    <ConnectedApp />
  </Provider>
);