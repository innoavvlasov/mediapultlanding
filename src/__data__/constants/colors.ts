export const Colors = Object.freeze({
    mainColor: '#2ea2f8',
    lightMain: '#00aeef',
    notActiveColor: '#ced0da',
    grayColor: '#848c98',
    white: 'white',
    orangeColor: '#ff7800',
    graishListBg: '#f1f4f8',
    alternativeGraish: '#eff3f6',
    bluish: '#354052',
    PrimeBlue: '#02abee'
});