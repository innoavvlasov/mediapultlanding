const pkg = require("./package.json");

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${pkg.version}/`
    }
  },
  config: {
    'main.api.base.url': '/api',
  },
  features: {
    feature1: true,
    feature2: false,
  },
  navigations: {
    news: "/news",
    orgs: "/orgs",
    ya: "https://yandex.ru"
  }
};
